import { Container, Grid, TableContainer, Table, TableHead, TableRow, TableCell, TableBody, Pagination, Button } from "@mui/material";
import { useEffect, useState } from "react";

function Datatable() {
    const limit = 10;
    
    const [rows, setRows] = useState([]);
    const [page, setPage] = useState(1);
    const [noPage, setNoPage] = useState(0);

    const changePageHandler = (event, value) => {
        setPage(value);
    }

    const fetchAPI = async (url) => {
        const response = await fetch(url);

        const data = await response.json();

        return data;
    }

    const detailButtonHandler = (row) => {
        console.log(row);
    }

    useEffect(() => {
        fetchAPI("https://jsonplaceholder.typicode.com/posts")
            .then((data) => {
                setNoPage(Math.ceil(data.length / limit));

                setRows(data.slice((page - 1) * limit,  page * limit));
            })
            .catch((error) => {
                console.error(error.message)
            })
    }, [page])

    return(
        <Container>
            <Grid container>
                <Grid item>
                    <TableContainer>
                        <Table aria-label="simple table">
                            <TableHead>
                                <TableRow>
                                    <TableCell align="center">Id</TableCell>
                                    <TableCell align="center">User ID</TableCell>
                                    <TableCell align="center">Title</TableCell>
                                    <TableCell align="center">Body</TableCell>
                                    <TableCell align="center">Action</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {
                                    rows.map((row, index) => {
                                        return (
                                            <TableRow key={index}>
                                                <TableCell align="center">{row.id}</TableCell>
                                                <TableCell align="center">{row.userId}</TableCell>
                                                <TableCell>{row.title}</TableCell>
                                                <TableCell>{row.body}</TableCell>
                                                <TableCell><Button variant="outlined" onClick={() => {detailButtonHandler(row)}}>Chi tiết</Button></TableCell>
                                            </TableRow>
                                        )
                                    })
                                }
                            </TableBody>
                        </Table>
                    </TableContainer>
                </Grid>
            </Grid>
            <Grid container mt={3} mb={2} justifyContent="flex-end">
                <Grid item>
                    <Pagination count={noPage} defaultPage={page} onChange={changePageHandler}/>
                </Grid>
            </Grid>
        </Container>
        
    )
}

export default Datatable;